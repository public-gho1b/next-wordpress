import { API_URL, API_REFRESH_TOKEN } from '@/constants';

export default async function FetchApi<T>(
  query: string,
  { variables }: Record<string, any> = {},
): Promise<T> {
  const headers: HeadersInit = { 'Content-Type': 'application/json' };
  if (API_REFRESH_TOKEN) {
    headers.Authorization = `Bearer ${API_REFRESH_TOKEN}`;
  }

  return fetch(API_URL, {
    headers,
    method: 'POST',
    body: JSON.stringify({ query, variables }),
  })
    .then((res) => res.json())
    .then((json) => {
      if (json.errors) {
        // eslint-disable-next-line no-console
        console.log(json.errors);
        throw new Error(json.errors);
      }
      return json.data;
    });
}
