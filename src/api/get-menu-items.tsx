import fetchApi from './fetch-api';

interface Data {
  menuItems: MenuItems;
}

interface MenuItems {
  nodes: Node[];
}

interface Node {
  id: string;
  parentId?: string;
  label: string;
  uri: string;
  order: number;
}

export interface MenuItem {
  id: string;
  parentId?: string;
  title: string;
  uri: string;
  order: number;
  childrens: MenuItem[];
}

export default async function getMenuItems() {
  return fetchApi<Data>(
    `query getMenuItems {
        menuItems {
          nodes {
            id
            parentId
            label
            uri
            order
          }
        }
      }`,
  )
    .then((data) => data.menuItems.nodes)
    .then((menuItems) => {
      const tree: MenuItem[] = [];
      const childrenOf: { [key: string]: MenuItem[] } = {};
      menuItems.forEach((menuItem) => {
        const newItem: MenuItem = {
          title: menuItem.label,
          childrens: [],
          ...menuItem,
        };
        const { id, parentId = 0 } = newItem;
        childrenOf[id] = childrenOf[id] || [];
        newItem.childrens = childrenOf[id];
        if (parentId) {
          childrenOf[parentId] = childrenOf[parentId] || [];
          childrenOf[parentId].push(newItem);
        } else {
          tree.push(newItem);
        }
      });
      return tree;
    });
}
