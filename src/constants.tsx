const { WORDPRESS_API_URL, WORDPRESS_AUTH_REFRESH_TOKEN } = process.env;

export const WEBSITE_NAME = 'Anggit Logs';
export const API_URL = WORDPRESS_API_URL ?? 'https://angg.it/graphql';
export const API_REFRESH_TOKEN = WORDPRESS_AUTH_REFRESH_TOKEN;
