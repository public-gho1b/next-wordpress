export default async function Category(props: {
  params: { category: string };
}) {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <article className="prose lg:prose-xl">
        <h1>{props.params.category}</h1>
      </article>
    </main>
  );
}
