export default async function Post({ params: { post } }) {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <article className="prose lg:prose-xl">
        <h1>{post}</h1>
      </article>
    </main>
  );
}
