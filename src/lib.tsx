// eslint-disable-next-line import/prefer-default-export
export const classNames = (...classes: string[]) => classes.join(' ');
