import * as constants from '@/constants';

const pairs = Object.fromEntries(Object.entries(constants));

const checkConstantValue = (key: string) => {
  it(`Constant ${key} exists`, () => expect(!!pairs[key]).toBeTruthy());
};

describe('Constants', () => {
  checkConstantValue('WEBSITE_NAME');
  checkConstantValue('API_URL');
});
